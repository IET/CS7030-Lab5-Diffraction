import numpy as np
import scipy
from scipy import signal
import pylab
from scipy.io import wavfile
import matplotlib.pyplot as plt
from matplotlib import animation
import matplotlib.cm as cm
import sys
import math

#Distanve From Aperature
z = 1879.6999
wavelength = 532 #nm (x10-9??)
#Quantities chosen to allow scaling factor to be one

#aperture = #A shape. 2D

starting_point = -3
time_to_plot = 6 # second

def rect(t, length=0.5):
	if abs(t) <= length:
		return 1
	elif abs(t) > length:
		return 0

def rect2D(x,y):
	return rect(x)*rect(y)

#x**2 + y**2 = r**2
def circ(x,y,radius=0.01):
	if ((x**2 + y**2) < radius):
		return 1
	else:
		return 0

#Trying other shapes
def tri(x, y, length=0.1):
	if ((abs(x) <= length) and (y < (length - abs(x))) and (y > 0)):
		return 1
	else:
		return 0

def G(x,y):
	return (np.exp(1j*k*z) * np.exp(1j*(k / (2*z) ) * (x**2 + y**2)))/(1j * wavelength * z) * np.fft.fft2(aperture)

def scale(list_2D, factor=10):
	maxVal = max(list_2D[len(list_2D)/2])
	for i,row in enumerate(list_2D):
		list_2D[i] = [x if (x < maxVal/factor) else (maxVal/factor + np.log10(x)) for x in row]
	return list_2D

def calc_power_spectra(signal, title):
	#My attempt at 3D plots, must come back to this
	#fig, axarr = plt.subplots(2,2)
	#axarr[0,0] = fig.gca(projection='3d')

	#X, Y = np.meshgrid(X, Y)

	#axarr[0,0].plot_surface(X, Y, Z)
	
	#plt.show()
	fig, axarr = plt.subplots(2,2)
	fig.suptitle(title, fontsize=20)

	axarr[0,0].set_title("Spatial Domain")
	axarr[0,0].imshow(signal, cmap=cm.Greys_r)
	
	fft_output = np.fft.fft2(signal)
	
	axarr[1,0].set_title("Frequency Domain")
	axarr[1,0].imshow(scale(np.real(np.fft.fftshift(fft_output))), cmap=cm.Greys_r)
	

	pow_spec = (np.abs(fft_output))**2
	#pow_spec = pow_spec * 10

	maxVal = max(pow_spec[len(pow_spec)/2])
	for i,row in enumerate(pow_spec):
		pow_spec[i] = [x if (x < maxVal/10) else (maxVal/10 + np.log10(x)) for x in row]

	#pow_spec = pow_spec / 10
	
	axarr[1,1].set_title("Power Spectrum")
	axarr[1,1].imshow(np.fft.fftshift(scale(pow_spec)) , cmap=cm.Greys_r)

	plt.savefig("{0} Diffraction.png".format(title), dpi=96)
	plt.show()

function = rect2D
title = ""

while True:
	x = np.linspace(starting_point, starting_point + time_to_plot, (3)/0.005+1)
	y = np.linspace(starting_point, starting_point + time_to_plot, (3)/0.005+1)

	index = input("Please Choose An Aperture Shape:\
					\n1. Square\
					\n2. Circle\
					\n3. Triangle\
					\n4. Exit\
					\n\n")

	if index == '1':
		title ="Rectangular Aperture"
		function = rect2D
	elif index == '2':
		function = circ
		title = "Circular Aperture"
	elif index == '3':
		function = tri
		title = "Triangular Aperture"
	elif index == '4':
		sys.exit(1)
	else:
		print("Invalid Input, Please Choose From the available Options\n")
		continue

	z = []
	for i,k in enumerate(x):
		z.append([])
		for j in y:
			z[i].append(function(k, j))

	calc_power_spectra(z, title)